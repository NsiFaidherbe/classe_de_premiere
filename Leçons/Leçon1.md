![421421421 est un nombre premier en binaire, illustration de la série TV "Unité 42"](Illustrations/unite_42.png)
# Leçon n° 1 : Ecriture des entiers dans une base donnée.
------
## 1. Ecriture des entiers en base 10.
-------
Considérons le nombre entier suivant X = 1 234 506.  
Il s'agit d'un nombre entier écrit en base 10 ( On parle aussi d'écriture décimale ).  
On a :  
1 234 506 = 1 000 000 + 200 000 + 30 000 + 4 000 + 500 + 6  
1 234 506 = 1 &times; 10<sup>6</sup> + 2 &times; 10<sup>5</sup> + 3 &times; 10<sup>4</sup> + 4 &times; 10<sup>3</sup> +  5 &times; 10<sup>2</sup> + 0 &times; 10<sup>1</sup> + 6 &times; 10<sup>0</sup>   
Pour écrire un nombre en base 10, on a besoin des dix chiffres 0,1,2,3,...8,9.
> **Théorème**
>> Pour nombre entier n ,il existe une unique suite de nombres {a<sub>0</sub>;a<sub>1</sub>;a<sub>2</sub>...;a<sub>k</sub>} telle que :  
>> n = a<sub>k</sub> &times; 10<sup>k</sup> + a<sub>k-1</sub> &times; 10<sup>k-1</sup> + ... + a<sub>2</sub> &times; 10<sup>2</sup> + a<sub>1</sub> &times; 10<sup>1</sup> + a<sub>0</sub> &times; 10<sup>0</sup>  
>> avec a<sub>i</sub> &in;{0;1;2;...;8;9} pour tout i&in;{0;1;2....;k} et a<sub>k</sub> $`\not =`$ 0  
>> a<sub>k</sub>a<sub>k-1</sub>a<sub>k-2</sub>....a<sub>1</sub>a<sub>0</sub> représente alors l'écriture du nombre n en base 10.   
>> On peut alors écrire si la base 10 est la base utilisée par défaut :  
>> n = a<sub>k</sub>a<sub>k-1</sub>a<sub>k-2</sub>....a<sub>1</sub>a<sub>0</sub>   
>> ou pour préciser qu'il s'agit de l'écriture de n en base 10 :  
>> n= $`\overline{a_ka_{k-1}...a_2a_1a_0}^{10}`$  
>> X = 1 234 506 = $`\overline{1234506}^{10}`$ 

## 2. Ecriture des entiers dans d'autres bases.
-----
### A. Exemple d'écriture d'un nombre en base 5
On considère N = 34 = $`\overline{34}^{10}`$
On souhaite l'écrire en base 5 :  
> **Méthode 1 : par décomposition en puissances décroissantes**
>> On détermine les différentes puissances de la base dont on aura besoin :
>> $`5^0 = 1 ; 5^1 = 5 ; 5^2 = 25 ; 5^3 = 125`$  
>> $`5^3 = 125`$ ne sera pas nécessaire puisque $`N<125`$       
>> On décompose N suivant les puissances décroissantes de 5 nécessaires :  
>> 34 = $` 1 \times25 + 9`$  
>> 34 = $` 1 \times25 + 1 \times5 + 4 \times1`$  
>> 34 = $` 1 \times 5^2 + 1 \times5^1+ 4 \times5^0`$  
>> $`\overline{34}^{10} = \overline{114}^{5}`$  
_______
#### Exercices
{- Exercice 1 -}
1. Donner l'écriture en base 5 du nombre N = 100 = $`\overline{100}^{10}`$
2. Donner l'écriture en base 5 du nombre N = 200 = $`\overline{200}^{10}`$
3. On considère le nombre N = $`\overline{3201}^{5}`$.Donner son écriture en base 10.
4. Donner l'écriture en base 10 du plus grand nombre que l'on peut écrire avec 4 chiffres en base 5.

{- Exercice 2 -}  
Donner l'écriture en base 3 du nombre N = $`\overline{1021}^{5}`$

{- Exercice 3 -}  
Cocher l'unique bonne réponse parmi les 4 propositions.
1. Soit N=10 écrit en base 10. Son écriture en base 5 est :
    - [ ] $`\overline{2}^{5}`$
    - [ ] $`\overline{20}^{5}`$
    - [ ] $`\overline{21}^{5}`$
    - [ ] $`\overline{10}^{5}`$
2. Soit N=$`\overline{2}^{3}`$ écrit en base 3. Son écriture en base 10 est :
    - [ ] 2
    - [ ] 6
    - [ ] 9
    - [ ] 8
3. Soit N=$`\overline{322}^{5}`$ écrit en base 5. Son écriture en base 10 est :
    - [ ] 322
    - [ ] 30
    - [ ] 32
    - [ ] 87
4. Le plus grand nombre écrit avec 4 chiffres en base 3 est :
    - [ ] $`\overline{3000}^{3}`$
    - [ ] $`\overline{2222}^{3}`$
    - [ ] $`\overline{2000}^{3}`$
    - [ ] $`\overline{9999}^{3}`$
5. Soit N le plus grand nombre écrit avec 2 chiffres en base 5. L'écriture de N en base 10 est :
    - [ ] 9
    - [ ] 24
    - [ ] 6
    - [ ] 99