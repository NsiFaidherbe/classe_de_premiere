# Documentation Markdown sur Gitlab
Le rendu des fichiers Markdown sur GitlaB se fait à l'aide du moteur [lien vers le moteur redcarpet de rendu des fichiers markdown](https://github.com/vmg/redcarpet)   
documentation : [lien vers la documentation Markdown sur Gitlab](https://gitlab.com/help/user/markdown.md)
______________________________________
liste de tâches :  
- [x] Completed task
- [ ] Incomplete task
    - [ ] Sub-task 1
    - [x] Sub-task 2
    - [ ] Sub-task 3
________________________________________
Mathématiques :  
This math is inline $`a^2+b^2=c^2`$.

This is on a separate line
```math
a^2+b^2=c^2
```
on peut aussi utiliser  
<img src="https://latex.codecogs.com/svg.latex?\Large&space;x=\frac{-b\pm\sqrt{b^2-4ac}}{2a}" title="\Large x=\frac{-b\pm\sqrt{b^2-4ac}}{2a}" />  
qui insère une image au format svg générée par le code latex passé en x
[page relative à Katex le moteur de rendu Latex](https://katex.org/docs/supported.html)
______________________
couleurs :  
`#F00`
`#F00A`
`#FF0000`
`#FF0000AA`
`RGB(0,255,0)`
`RGB(0%,100%,0%)`
`RGBA(0,255,0,0.7)`
`HSL(540,70%,50%)`
`HSLA(540,70%,50%,0.7)`
________________________
Mermaid :  
```mermaid
graph TD;
  A-->B;
  A-->C;
  B-->D;
  C-->D;
```
______________________
