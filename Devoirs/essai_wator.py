########################################
# Wator : simulation proies,prédateurs #
########################################
'''
    Variables :
    : mer : (list) représentation du plateau de jeu,
    liste de longueur la hauteur du plateau, contenant des listes de longueur la largeur du plateau
    : contenu : (list) représentation du contenu de chaque case
    pour une case vide : contenu=["mer"]
    pour un case contenant un thon : contenu=["thon",gestation]
    pour une case contenant un requin : contenu = ["requin",gestation, énergie]
    : hauteur : (int) hauteur du plateau de simulation
    : largeur : (int) largeur du plateau
    : nbre_etapes : (int) nombre d'étapes de la simulation
    : evolution_mer : (list ) liste contenant l'état de la mer à chaque étape
    : comportement_initial : (dictionnaire) contient les valeurs définies pour le comportement de chaque espèce
    : comportement_initial["thon"]=["thon",gestation=2] : liste de l'etat initial pour le comportement défini pour les thons 
    : comportement_initial["requin"]=["requin",gestation=3,energie=5] : liste de l'etat initial pour le comportement défini pour les requins 
'''

from random import randint # importe du module random la fonction r andint
import matplotlib.pyplot as plt

def nbre_aleatoire(min,max) :
    '''
    tire un nombre entier aléatoire compris entre les valeurs min et max
    : min : (int) plus petite valeur possible
    : max : (int) plus grande valeur possible
    : return : (int) un nombre entier aléatoire compris entre les valeurs min et max 
    '''
    return randint(min,max)

def remplir_la_mer(largeur,hauteur,contenu) :
    '''
    Retourne une liste de n listes ( n : hauteur ) composée chacune de k zéros ( k : largeur )
    : largeur : (int) largeur de la mer
    : hauteur : (int) hauteur de la mer
    : effets de bords : remplit la liste de ["mer"]
    : 
    
    >>>remplir_la_mer(mer,3,2,["mer"])
    [[["mer"], ["mer"], ["mer"]], [["mer"], ["mer"], ["mer"]]]
    '''
    mer=[[contenu]*largeur for i in range(hauteur)]
    return mer
    
def test_contenu_case(mer,x,y,contenu):
    '''
    déterminer si la case de coordonnées x,y contient le contenu
    : mer : (list) liste représentant l'état de la mer
    : x : (int) abscisse de la case
    : y : (int) ordonnée de la case
    : contenu : (str) type de contenu ( mer, thon, requin ....)
    : effets de bords : aucun
    : return : (booleen) True si la case est vide, False sinon.
    '''
    if mer[y][x][0]==contenu :
        return True
    else :
        return False

def creation_population_poissons(mer,nbre_poissons,contenu):
    '''
    Remplit la mer du nombre de poissons désirés
    : mer : (list)
    : nbre_poissons : (int)
    : poisson : (list) liste décrivant le type de poissons et les valeurs nécessaires au comportement
    : Effets de bords : modifie la liste mer
    '''
    hauteur=len(mer)
    largeur=len(mer[0])
    #poisson=contenu.copy()
    for i in range(0,nbre_poissons):
        x=nbre_aleatoire(0,largeur-1) # on détermine aléatoirement les coordonnées de la case
        y=nbre_aleatoire(0,hauteur-1)
        while test_contenu_case(mer,x,y,"mer")!=True : # on détermine si la case est vide
            x=nbre_aleatoire(0,largeur-1) # sinon on détermine de nouvelles coordonnées
            y=nbre_aleatoire(0,hauteur-1)
        mer[y][x]=contenu.copy() # on place le poisson
    return mer

def coordonnees_cases_adjacentes(mer,x,y) :
    '''
    retourne la liste des coordonnées des cases adjacentes
    à la case (x,y) en tenant compte de la géométrie thorique
    : mer : (list)
    : x : (int) coordonnée horizontale de la case 
    : y : (int) coordonnée verticale  de la case
    : return : liste des coordonnées des 4 cases adjacentes
    : effets de bords : aucun
    '''
    coord_cases_adjacentes=[] # liste des coordonnées des cases adjacentes
    # on tient conpte de la géométrie théorique de la mer
    if x==0 : # on détermine les coordonnées de la case adjacente à gauche
        coord_cases_adjacentes.append([len(mer[0])-1,y])
    else :
        coord_cases_adjacentes.append([x-1,y])
    if y==0 :  # on détermine les coordonnées de la case supérieure
        coord_cases_adjacentes.append([x,len(mer)-1])
    else :
        coord_cases_adjacentes.append([x,y-1])
    if x==len(mer[0])-1 : # on détermine les coordonnées de la case adjacente à droite
        coord_cases_adjacentes.append([0,y])
    else :
        coord_cases_adjacentes.append([x+1,y])
    if y==len(mer)-1 : # on détermine les coordonnées de la case inférieure
        coord_cases_adjacentes.append([x,0])
    else :
        coord_cases_adjacentes.append([x,y+1])
    return coord_cases_adjacentes
    
def case_adjacente_aleatoire_ayant_contenu(mer,x,y,contenu):
    '''
    détermine les cases libres autour d'une case de coordonnées x,y
    : mer : (list) plateau représentant la mer
    : x : (int) coordonnée horizontale de la case
    : y : (int) coordonné verticale de la case
    : contenu : (str) type du contenu 
    : return : (list) une liste contenant les coordonnées des cases libres aux alentours
    '''
    # on définit une liste contenant les éventuelles cases adjacentes ayant le contenu désiré
    cases_adjacentes=[]
    # on définit la case adjacente aléatoire ayant le contenu qui sera retournée
    case_adjacente_aleatoire=[]
    # on détermine les coordonnées des cases adjacentes
    coord_cases_adjacentes=coordonnees_cases_adjacentes(mer,x,y)
    # on détermine quelles sont les cases adjacentes ayant le contenu déterminé
    for i in range(0,4) :
        x = coord_cases_adjacentes[i][0] # abscisse de la case adjacente
        y = coord_cases_adjacentes[i][1] # ordonnée de la case adjacente
        if test_contenu_case(mer,x,y,contenu)== True : # si la case contient le contenu recherché
            cases_adjacentes.append([x,y]) # on ajoute les coordonnées à la liste des cases adjacentes
    # on déterminer aléatoirement une case adjacente ayant le contenu voulu
    if not(len(cases_adjacentes)==0) : # si il y a des cases adjacentes avec le contenu désiré
            choix_aleatoire=nbre_aleatoire(0,len(cases_adjacentes)-1) # on choisit aléatoirement une case
            case_adjacente_aleatoire=cases_adjacentes[choix_aleatoire]
    return case_adjacente_aleatoire

def comportement_thon(mer,x,y,comportement_initial) :
    '''
    gère le comportement spécifiques des thons
    : mer : (list)
    : poisson : (list) contenant ["thon",gestation]
    : return : mer
    : effet de bord : modifie mer
    '''
    print("evolution thon",x,y)
    affichage_mer(mer,x,y)
    thon=mer[y][x] # on récupère le contenu de la case
    case_libre=case_adjacente_aleatoire_ayant_contenu(mer,x,y,"mer") # On déterminer la liste des cases vides aux alentours
    if not(len(case_libre)==0) : # déplacement du thon si une case est libre
            nouveau_x=case_libre[0] # on mémorise les nouvelles coordonnées
            nouveau_y=case_libre[1]
            thon[1]-=1 # on enlève 1 à la gestation
            if thon[1]==0 : # si la gestation était égale à 0,
                mer[y][x]=comportement_initial["thon"].copy() # on ajoute un nouveau thon à l'ancienne position
                thon[1]=comportement_initial["thon"].copy()[1] # on remet la gestation du thon à la valeur initiale
            else :
                mer[y][x]=comportement_initial["mer"].copy() # sinon remet le contenu à mer
            mer[nouveau_y][nouveau_x]=thon.copy()
            affichage_mer(mer,nouveau_x,nouveau_y)
    return mer

def comportement_requin(mer,x,y,comportement_initial) :
    '''
    gère le comportement spécifiques des thons
    : mer : (list)
    : poisson : (list) contenant ["thon",gestation]
    : return : mer
    : effet de bord : modifie mer
    '''
    print("evolution requin",x,y)
    affichage_mer(mer,x,y)
    requin=mer[y][x] # on récupère le contenu de la case
    print("etat du requin",requin)
    requin[2]-=1 # le requin perd un point d'énergie
    print(" perte énergie",requin)
    
    # on détermine s'il y a des thons sur les cases adjacentes
    case_libre=case_adjacente_aleatoire_ayant_contenu(mer,x,y,"thon") # On détermine une case aléatoire adjacente ayant un thon
    print("case avec thon",case_libre)
    if not(len(case_libre)==0) : # déplacement du requin si une case contient un thon
            nouveau_x=case_libre[0] # on mémorise les nouvelles coordonnées
            nouveau_y=case_libre[1]
            requin[2]=comportement_initial["requin"].copy()[2] # on remet l'énergie à la valeur initiale
            requin[1]-=1 # on diminue la gestation
            if requin[1]==0 : # si la gestation était égale à 0,
                mer[y][x]=comportement_initial["requin"].copy() # on ajoute un nouveau requin à l'ancienne position
                requin[1]=comportement_initial["requin"].copy()[1] #  on remet la gestation à la valeur initiale
            else :
                mer[y][x]=comportement_initial["mer"].copy() # sinon on supprime le requin de l'ancienne case
            mer[nouveau_y][nouveau_x]=requin.copy() # on déplace le requin à la nouvelle case
            print("nouvelle position",nouveau_x,nouveau_y)
            print("nouvel état",requin)
            affichage_mer(mer,nouveau_x,nouveau_y)
            return mer # on renvoit le contenu de la mer
    # on détermine si le requin est mort
    if requin[2]==0 :
        mer[y][x]=comportement_initial["mer"].copy() # on supprime le requin de l'ancienne case
        return mer # on renvoit le contenu de la mer
    
    # on détermine s'il y a des cases vides adjacentes
    case_libre=case_adjacente_aleatoire_ayant_contenu(mer,x,y,"mer") # On détermine une case aléatoire vide
    print("case vide",case_libre)
    if not(len(case_libre)==0) : # déplacement du requin si une case contient une case vide
            nouveau_x=case_libre[0] # on mémorise les nouvelles coordonnées
            nouveau_y=case_libre[1]
            requin[1]-=1 # on diminue la gestation
            if requin[1]==0 : # si la gestation était égale à 0,
                mer[y][x]=comportement_initial["requin"].copy() # on ajoute un nouveau requin à l'ancienne position
                requin[1]=comportement_initial["requin"].copy()[1] #  on remet la gestation à la valeur initiale
            else :
                mer[y][x]=comportement_initial["mer"].copy() # sinon on supprime le requin de l'ancienne case
            mer[nouveau_y][nouveau_x]=requin.copy() # on déplace le requin à la nouvelle case
            affichage_mer(mer,nouveau_x,nouveau_y)
            print("nouvelle position",nouveau_x,nouveau_y)
            print("nouvel état",requin)
            return mer
    print("dernier état",requin)
    print("nouvelle position",nouveau_x,nouveau_y)
    affichage_mer(mer,nouveau_x,nouveau_y)
    return mer
     
            
    
    
def evolution_case(mer,x,y,comportement_initial) :
    '''
    déplacement aléatoire des thons et naissances éventuelles
    : mer : (list)
    : x,y : (int) (int) coordonnées de la case contenant le poisson
    '''
    contenu=mer[y][x] # on récupère le contenu de la case
    if contenu[0]=="thon": # si la case contient un thon
                mer=comportement_thon(mer,x,y,comportement_initial)
    if contenu[0]=="requin": # si la case contient un thon
                mer=comportement_requin(mer,x,y,comportement_initial)
    return mer
    
def affichage_mer(mer,x,y) :
    '''
    affiche l"état de la mer à chaque évolution
    : evolution mer : (list) liste contenant les évolutions successives
    '''
    x_thons=[] # abscisses des thons
    y_thons=[] # ordonnées des thons
    x_requins=[] # abscisses des requins
    y_requins=[] # ordonnées des requinsy]
    x_poisson=[]
    y_poisson=[]
    taille_thons=[]
    x_poisson.append(x)
    y_poisson.append(y)
    if mer[y][x][0]=="thon" :
        plt.scatter(x_poisson,y_poisson, s= 160, c='green',marker="o")
    if mer[y][x][0]=="requin" :
        plt.scatter(x_poisson,y_poisson, s= 160, c='green',marker="x")
    for y in range(0,len(mer)):
        for x in range(0,len(mer[0])):
            if test_contenu_case(mer,x,y,"thon") :
                x_thons.append(x);
                y_thons.append(y);
            if test_contenu_case(mer,x,y,"requin") :
                x_requins.append(x);
                y_requins.append(y);
    axes = plt.gca()
    axes.set_xlim(-0.5,len(mer[0])-0.5)
    axes.set_ylim(-0.5,len(mer)-0.5)
    plt.scatter(x_thons,y_thons, s= 80, c='blue',marker="o")
    plt.scatter(x_requins,y_requins, s= 80, c='red',marker="x")
    plt.show()
    
    

        
        
                                     
    
    


def simulation(largeur,hauteur,nbre_thons,nbre_requins,nbre_etapes):
    '''
    lancement de la simulation
    : largeur : (int) largeur de la mer
    : hauteur : (int) hauteur de la mer
    : nombre_thons : (int)
    : nbre_etapes : (int) nombre d'étapes de la simulation
    : return : affichage de l'évolution
    : effets de bords : affichage,modification des listes
    '''
    comportement_initial = {
        "mer" : ["mer"],
        "thon" : ["thon",2],
        "requin": ["requin",3,5]
        }
    
    evolution_mer=[] # liste contenant les évolutions successives de la mer
    mer=remplir_la_mer(largeur,hauteur,comportement_initial["mer"]) # on remplit la mer
    mer=creation_population_poissons(mer,nbre_thons,comportement_initial["thon"]) # on ajoute les thons
    mer=creation_population_poissons(mer,nbre_requins,comportement_initial["requin"]) # on ajoute les requins
    affichage_mer(mer,0,0)
    for i in range(0,nbre_etapes) : # on effectue l'evolution sur le nombre d'étapes désirés
        x=nbre_aleatoire(0,len(mer[0])-1) # on tire les coordonnées d'une case au hasard
        y=nbre_aleatoire(0,len(mer)-1)
        mer=evolution_case(mer,x,y,comportement_initial) # on évolue suivant le contenu de la case
        
    return mer
    
   
simulation(10,10,5,3,1000)

        
        
    
    
    
    
    
    
        
        
        
        
    
    
